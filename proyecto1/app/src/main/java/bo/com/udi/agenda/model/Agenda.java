package bo.com.udi.agenda.model;

import java.util.ArrayList;
import java.util.List;

public class Agenda {

    private String titulo;

    private List<Tarea> tareaList;

    public Agenda(String titulo) {
        this.titulo = titulo;

        this.tareaList = new ArrayList<>();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Tarea> getTareaList() {
        return tareaList;
    }

    public void setTareaList(List<Tarea> tareaList) {
        this.tareaList = tareaList;
    }
}
