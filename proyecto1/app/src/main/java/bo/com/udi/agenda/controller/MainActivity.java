package bo.com.udi.agenda.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import bo.com.udi.agenda.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void logIn(View view) {
        String usr = ((EditText)findViewById(R.id.idUserEmail)).getText().toString();
        String pwd = ((EditText)findViewById(R.id.idUserPwd)).getText().toString();

        TextView message = findViewById(R.id.idMessage);

        if (usr.isEmpty() || pwd.isEmpty()) {
            message.setVisibility(View.VISIBLE);
            return;
        }

        message.setVisibility(View.INVISIBLE);

        startActivity(new Intent(this, NuevaLIstaTareas.class));
    }
}
