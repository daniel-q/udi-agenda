package bo.com.udi.agenda.controller;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import bo.com.udi.agenda.R;
import bo.com.udi.agenda.model.Agenda;
import bo.com.udi.agenda.model.Tarea;

public class TareaAdapter extends BaseAdapter {

    private Agenda agenda;

    private Context context;

    public TareaAdapter(Context context, Agenda agenda) {

        this.context = context;
        this.agenda = agenda;
    }

    @Override
    public int getCount() {
        return this.agenda.getTareaList().size();
    }

    @Override
    public Object getItem(int i) {
        return this.agenda.getTareaList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = View.inflate(this.context, R.layout.item_tarea, null);
            new ViewHolder(convertView);
        }

        Tarea tarea = (Tarea) getItem(position);

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.titulo.setText(tarea.getTitulo());
        viewHolder.descripcion.setText(tarea.getDescripcion());

        return convertView;
    }

    private  class ViewHolder {

        private TextView titulo;

        private TextView descripcion;

        public ViewHolder(View view) {
            titulo = view.findViewById(R.id.idTituloTareaItem);
            descripcion = view.findViewById(R.id.idDescripcionTareaItem);
            view.setTag(this);
        }
    }
}
