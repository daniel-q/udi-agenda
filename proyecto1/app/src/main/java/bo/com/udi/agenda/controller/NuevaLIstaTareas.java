package bo.com.udi.agenda.controller;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import bo.com.udi.agenda.R;
import bo.com.udi.agenda.model.Agenda;
import bo.com.udi.agenda.model.Tarea;

public class NuevaLIstaTareas extends AppCompatActivity {

    private Agenda agenda;

    private TareaAdapter tareaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_lista_tareas);

        this.agenda = new Agenda("Agenda");

        initTarea();

        this.tareaAdapter = new TareaAdapter(this, this.agenda);

        ((ListView) findViewById(R.id.nuevaTareaList)).setAdapter(this.tareaAdapter);
    }

    private void initTarea() {
//        this.agenda.getTareaList().add(new Tarea("Tarea 1", "Descripcion 1"));
//        this.agenda.getTareaList().add(new Tarea("Tarea 2", "Descripcion 2"));
//        this.agenda.getTareaList().add(new Tarea("Tarea 3", "Descripcion 3"));
//        this.agenda.getTareaList().add(new Tarea("Tarea 4", "Descripcion 4"));

        DatabaseReference root = FirebaseDatabase.getInstance().getReference().child("agenda");

        root.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                HashMap<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();

                Tarea tarea = new Tarea(map.get("title"), map.get("description"));

                agenda.getTareaList().add(tarea);

                tareaAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
